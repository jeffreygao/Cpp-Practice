//
// Created by gaoje on 5/4/2021.
// Chapter 5 general content
// Using sequential containers and analyzing stringss
//
#include "Student_info.h"
#include "grade.h"
#include <vector>
#include <list>
#include <string>
#include <cctype> // isspace
#include <iostream>

using std::list;
using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::cin;


// check whether student is failing
bool fgrade(const Student_info& s) {
    return grade(s) < 60;
}

/*
 * ----- Vector note
 * erase() will invalidate iterators refering to erased element and subsequent elements
 * push_back() will invalidate entire vector
 * Take note when keeping saved copies of iterators.
 */


// Extract failing students
// -- Erasing elements in place: avoid data copies
vector<Student_info> extract_fails_0(vector<Student_info>& students) {
    vector<Student_info> pass, fail;
    for (vector<Student_info>::size_type i = 0;
    i != students.size(); i++) {
        if (fgrade(students[i]))
            fail.push_back(students[i]);
        else
            pass.push_back(students[i]);
    }
    students = pass;
    return fail;
}

/*
 * ----- RUNTIME
 *
 * Expensive to add or remove from vector(optimized for random access)
 * Remove all, runtime O(n^2)
 *
 * If we know we do not need to use indices(ability to access element randomly),
 * we can use _Iterator_ which supports sequential access efficiently
 */

// ----- ITERATOR
// ## Using random access methods sequentially
// for (vector<Student_info>::size_type i = 0;
//     i != students.size(); ++i)
//     cout << students[i].name << endl;
//
// ## Restrict operations to only what the container can handle efficiently
// for (vector<Student_info>::const_iterator iter = students.begin();
//     iter != students.end(); ++iter) {
//     cout << (*iter).name << endl;
// }

// container-type::const_iterator
// container_type::iterator

/*
 * ----- DEREFERENCE Operator *
 *
 * Operator returns an lvalue that is referenced element
 * E.g. (*iter).name would refer to the name member of the *iter object
 *
 * (*iter) is a reference object so instead, we can write
 *      iter->name
 */


// version 3: iterators but no indexing; still potentially slow
vector<Student_info> extract_fails(vector<Student_info>& students) {
    vector<Student_info> fails;
    vector<Student_info>::iterator iter = students.begin(); // not using const because we modify with erase
    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fails.push_back(*iter);
            iter = students.erase(iter); // iter refers to the element after the erasure
            // removing element denoted by iter also invalidates the iterator, thus must reassign
        } else
            ++iter;
    }
    return fails;
}


// ----- List
// Optimized for fast insert and delete, maintains a more complex structure
// List does not use indexing

// version 4: use list instad of vector
list<Student_info> extract_fails_1(list<Student_info>& students) {
    list<Student_info> fail;
    list<Student_info>::iterator iter = students.begin();
    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fail.push_back(*iter);
            iter = students.erase(iter);
        } else
            iter++;
    }
    return fail;
}


// ----- To sort a list
// ## Use the member function sort instead of global sort
// 1) students.sort(compare)
// 2) sort(students.begin(), students.end(), compare)


// ----- STRINGS
// Can be thought of as container of characters
// - iterator
// - indexing

vector<string> split(const string& s)
{
    vector<string> ret;
    typedef string::size_type string_size; // appropriate type for indexing string
    string_size i = 0;
    // invariant: we have processed characters [original value of i, i)
    while (i != s.size()) {
        // ignore leading blanks
        // invariant: characters in range [original i, current i) are all spaces
        while (i != s.size() && isspace(s[i]))
            ++i;

        // find end of next word
        string_size j = i;
        // invariant: none of the characters in range [original j, current j) is a space
        while (j != s.size() && !isspace(s[j]))
            ++j;

        // if we found some nonwhitespace characters
        if (i != j) {
            // copy from s starting at i and taking j – i chars
            ret.push_back(s.substr(i, j - i));
            i = j;
        }
    }
    return ret;
}


int main() {
    string s;
    while (cin >> s)
        cout << s << endl;
    return 0;
}
