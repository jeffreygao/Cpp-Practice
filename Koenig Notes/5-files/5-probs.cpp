//
// Created by gaoje on 5/5/2021.
//
#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
#include <iomanip> // setw()

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::sort;
using std::vector;
using std::setw;


vector<string> split(const string& s)
{
    vector<string> ret;
    typedef string::size_type string_size; // appropriate type for indexing string
    string_size i = 0;
    // invariant: we have processed characters [original value of i, i)
    while (i != s.size()) {
        // ignore leading blanks
        // invariant: characters in range [original i, current i) are all spaces
        while (i != s.size() && isspace(s[i]))
            ++i;

        // find end of next word
        string_size j = i;
        // invariant: none of the characters in range [original j, current j) is a space
        while (j != s.size() && !isspace(s[j]))
            ++j;

        // if we found some nonwhitespace characters
        if (i != j) {
            // copy from s starting at i and taking j – i chars
            ret.push_back(s.substr(i, j - i));
            i = j;
        }
    }
    return ret;
}


struct aline {
    vector<string> rotations;
    string first;
    string pivot;
};


bool compare(const aline& line1, const aline& line2) {
    return line1.rotations[0] < line2.rotations[0];
}


void permutation() {
    // Get input phrases
    vector<string> input;
    string s;
    while (getline(cin, s)) {
        if (!s.empty())
            input.push_back(s);
        else
            break;
    }

    typedef vector<string>::size_type size;

    vector<aline> rotations;
    for (size i = 0; i != input.size(); i++) {
        vector<string> temp = split(input[i]);
        auto iter = temp.begin(); // iterator type
        aline line;
        line.rotations = temp;
        line.first = *temp.begin();
        rotations.push_back(line);
        string last = *temp.rbegin();
        while (*iter != last) {
            temp.push_back(*temp.begin());
            iter = temp.erase(temp.begin());
            aline temp_line;
            temp_line.rotations = temp;
            temp_line.first = line.first;
            rotations.push_back(temp_line);
        }
    }

    sort(rotations.begin(), rotations.end(), compare);

    for (size i = 0; i != rotations.size(); i++) {
        // rotate back
        vector<string>& rotation = rotations[i].rotations;
        const string first = rotations[i].first;
        const string pivot = rotation[0];
        rotations[i].pivot = pivot;
        if (rotation[0] != first) {
            auto iter = rotation.begin();
            while (*iter != first) {
                rotation.push_back(*rotation.begin());
                iter = rotation.erase(rotation.begin());
            }
        }
    }

    for (size i = 0; i != rotations.size(); i++) {
        int len = rotations[i].rotations.size();
        for (size j = 0; j != rotations[i].rotations.size(); j++) {
            if (rotations[i].rotations[j] == rotations[i].pivot) {
                cout << setw(len-4*j) << rotations[i].rotations[j] << " ";
            } else
                cout << rotations[i].rotations[j] << " ";
        }
        cout << endl;
    }
}


void prob_5(const vector<string>& pic) {
    for (const string& i : pic) {
        cout << i.substr(0, 4) << setw(5) << endl;
    }
}


int main() {
//    permutation();
    vector<string> pic(5, "asdasdasd");
    prob_5(pic);
    return 0;
}

