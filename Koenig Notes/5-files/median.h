//
// Created by gaoje on 5/4/2021.
//

#ifndef ACCELERATED_CPP_KOENIG_MEDIAN_H
#define ACCELERATED_CPP_KOENIG_MEDIAN_H

#include <vector>

double median(std::vector<double>);

#endif //ACCELERATED_CPP_KOENIG_MEDIAN_H
