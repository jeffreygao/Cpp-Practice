//
// Created by gaoje on 4/30/2021.
//
#include <iostream>
#include <cmath>
#include <istream>
#include <iomanip>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::istream;
using std::streamsize;
using std::setw;
using std::setprecision;


void prob_1() {
    // Will this code work?
//    int maxlen;
//    Student_info s;
//    max(s.name.size(), maxlen);
    // Depends on implementation of max(). Is there a definition of this method with the correct input parameter types?
}

void prob_2(int n) {
    cout << setw(2) << n << " " << setw(4) << pow(n, 2) << endl;
}


void prob_4(double n) {
    streamsize size = cout.precision();
    cout << setprecision(4) << n << " " << pow(n, 2) << setprecision(size) << endl;
}


void prob_7() {
    std::vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8};
    double sum = 0;
    double avg;
//    for (int i = 0; i < nums.size(); i++) {
//        sum += nums[i];
//    }
    for (int i : nums) {
        sum += i;
    }
    avg = sum/nums.size();
    cout << avg << endl;
}

/*
 * double d = f()[n];
 * Can infer that return type of f() is a vector.
 */


int main() {
//    prob_2(56);
//    prob_2(14);
//    prob_4(15.5);
    prob_7();
    return 0;
}