//
// Chapter 7
// Using associative containers
//
#include <map>
#include <cstdlib> // rand()
#include <stdexcept> // domain error
#include <iostream>

using std::domain_error;
using std::rand;
using std::cout;


// Containers that support efficient look-up
// - associative containers are self-ordering, cannot change order of elements
// - value-initialized: set to 0
// - data structure Pair<const K,V>: has ->first and ->second element
// Note: lvalue is memory address; rvalue is the data value stored at the address

// ret[*it].push_back(line_number);
// - map ret
// - iterator it denotes elements of a vector(*it is a word)
// - vector at position indexed by [*it]
// - vector<int> adds line_number

// return a random integer in the range [0, n)
int nrand(int n)
{
    if (n <= 0 || n > RAND_MAX)
        throw domain_error("Argument to nrand is out of range");
        // RAND_MAX is limited to 32767(12-bit) but could be much larger to 32 or 64 bit
    const int bucket_size = RAND_MAX / n;
    int r;
    // do-while statement ensures body runs at least once
    do r = rand() / bucket_size; // generates int [0, RAND_MAX]
    while (r >= n);
    return r;
}



int main() {
    cout << RAND_MAX;
    return 0;
}