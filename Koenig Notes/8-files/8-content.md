# Chapter 8
### Writing generic functions

Template functions: implements generic functions
- allows sharing of common behavior, even between types
- based on template parameters, instantiates type-specific code automatically
- typename: specify a type e.g. size_type
- Instantiation occurs at link time (edit - compile - link)
- To instantiate template, definition and declaration must both be accessible.

Consider:
```cpp
template <class T>
T max(const T& left, const T& right) {
    return left > right ? left : right;
}
```
- No way to infer argument types if passed an int and double
 
## Iterator Types
There exists 5 iterators categories, corresponding to specific iterator operations.
These categories help define which containers can use which algorithms.
E.g. algorithms that need one or multiple passes -or- access arbitrary elements

### 1. Sequential read-only access
Only need to support ++, ==, !=, unary *, ->
- also called input operator if a type supports all these operators
- e.g. `iter->member == (*iter).member`

### 2. Sequential write-only access
Uses the output iterator
- takes 3 iterators as input (begin, end, destination)
- Can =,++
- can only execute ++ once between assignments, so as to not leave a gap in a data structure

### 3. Sequential read-write access
Forward iterator
*it (for both reading and writing)
++it and it++ (but not --it or it--)
it == j and it != j (where j has the same type as it)
it->member (as a synonym for (*it).member)

### 4. Reversible access
Bidirectional iterator
- uses begin, end, -- for traversing backwards

### 5. Random access
Does not include == or !- since this includes support for bidirectional iterators
p+n, p-n, and n+p
p-q
p[n] (equivalent to *(p + n))
p<q, p>q, p <= q, and p >= q


Why off-the-end value for iterator?
1. provides element to mark the end if empty structure
2. allows us to compare iterators only for equality and inequality
3. indicates out of range

## Template Function
`template <class type-parameter [, class type-parameter] ... >`

E.g. `template <class T> T zero() { return 0; }`
This defines a zero function to be a template function with a single type parameter.
Called by `double x = zero<double>();`
