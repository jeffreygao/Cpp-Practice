# Chapter 11: Defining abstract data types
Concerned with how to copy, assign, and destroy objects.

## Implementing the Vec class
```c++
template <class T> class Vec {
    public:
        // interface
    private:
        // implementation
}
```
Code says that Vec is a template class, with one type parameter named T.

> What information about the array to store?

Hold elements in a dynamically allocated array. Only need to hold address of first
and one past the last element.
```c++
private:
    T* data;  // first element in Vec
    T* limit; // one past the last element in Vec
``` 
Type T will be replaced by compiler with whatever type is used.

### Class components
#### Memory allocation
Instead of using T[n] to impose a memory allocation with default constructor,
use dedicated memory allocation class that allocates raw memory and then 
builds objects in that space.

#### Constructors
```c++
Vec<Student_info> vs; // default constructor
Vec<double> vs(100); // constructor that takes size

public:
    Vec() {
        create();
    }
    explicit Vec(std::size_t n, const T& val = T()) {
        create(n, val);
    }
```
The `create()` method is a private member that allocates memory. The `explicit`
word indicates that the constructor will only be used by the compiler
in which the user explicitly invokes the constructor. The type of the Vec object
cannot be implicitly interpreted as something else.

#### Types definitions
Want to define:
1. const/nonconst iterator types
1. value_type: type of each object stored in container
1. reference/const_reference: synonyms for value_type& and const value_type&
1. difference_type: type of the result of subtracting one iterator from another

We can use plain pointers as iterator type. Provides random-access-iterator
operations which is consistent with vector. Typically its own class.

```c++
template <class T> class Vec {
    public:
        typedef T* iterator; // added
        typedef const T* const_iterator; // added
        typedef size_t size_type; // added
        typedef T value_type; // added
        typedef std::ptrdiff_t difference_type; // added
        typedef T& reference; // added
        typedef const T& const_reference; // added

        Vec() { create(); }
        explicit Vec(size_type n, const T& val = T()) { create(n, val); }
        // remaining interface

    private:
        iterator data; // changed
        iterator limit; // changed
};
```

#### Index and size
__Overloaded operators:__
Formed by appending operator symbol to word "operator". Thus, function `operator[]`.
- "[]" is considered a function as it is called on a vector `vec[i]` with "i"
being passed as an argument. 
- Operand must be large enough to denote last element in largest vec, so Vec::size_type
- return reference to element stored in Vec, can read and write

```c++
public:
    Vec() { create(); }
    explicit Vec(size_type n, const T& val = T()) { create(n, val); }

    // new operations: size and index
    size_type size() const { return limit - data; }

    // return reference, operator function takes index as argument, finds
    // corresponding position in underlying array
    T& operator[](size_type i) { return data[i]; }
    const T& operator[](size_type i) const { return data[i]; }
```

p. 193 in-book
