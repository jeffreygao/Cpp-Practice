//
// Created by gaoje on 5/14/2021.
// Using library algorithms
//
#include <algorithm>
#include <vector>
#include <iterator> // iterator adaptor
#include <string>
#include <vector>
#include <string>
#include <cctype> // isalnum()
#include <numeric> // accumulate()

using std::string;
using std::copy;
using std::vector;


/*
 * generic algorithm:
 *  algo not part of any particular container but takes cues from its arguments' types
 *  E.g. Generic algo: copy(begin, end, out)
 */
void content_iterator() {
    /*
     * it = begin++;
     *
     * -- is equivalent to --
     *
     * it = begin;
     * begin++;
     */

    // Iterator adaptors
    // - depend on postfix version of increment operator
    // - <iterator> common back_inserter
    std::string bottom = "world";
    std::string ret = "hello";
    copy(bottom.begin(), bottom.end(), std::back_inserter(ret));  // copies and appends bottom to ret
    // Separate copying elements and expanding container to choose what operations to use
}


void content_split() {
    std::string top = "world";
    std::find_if(top.begin(), top.end(), "world");
    // First call seeks first nonspace character which begins the word.
    // index i,j used to delimit a word in string
}


void palindrome(const std::string& s) {
    std::equal(s.begin(), s.end(), s.rbegin());
}


// URL: uniform resource locator
// protocol-name://resource-name
vector<string> find_urls(const string& s) {
    vector<string> ret;
    typedef string::const_iterator iter;
    iter b = s.begin(), e = s.end();

    // look through entire output
    while (b != e) {
        // look for one or more letters followed by ://
        b = url_beg(b, e);

        // if url found
        if (b != e) {
            iter after = url_end(b, e);
            // save url
            ret.push_back(string(b, after));
            // move b up past url
            b = after;
        }
    }
    return ret;
}

bool not_url_char(char c)
{
    // characters, in addition to alphanumerics, that can appear in a URL
    static const string url_ch = "~;/?:@=&$-_.+!*’(),";
    // STATIC: storage class specifier
    // construct and initialize on first call, subsequent calls will use first object constructed

    // see whether c can appear in a URL and return the negative
    return !(isalnum(c) ||
             find(url_ch.begin(), url_ch.end(), c) != url_ch.end()); // returns iterator at c instead of bool > find_if
}

string::const_iterator url_end(string::const_iterator b, string::const_iterator e)
{
    return find_if(b, e, not_url_char);  // not_url_char is the comparator function
}

string::const_iterator
url_beg(string::const_iterator b, string::const_iterator e)
{
    static const string sep = "://";
    typedef string::const_iterator iter;
    // i marks where the separator was found
    iter i = b;
    while ((i = search(i, e, sep.begin(), sep.end())) != e) {
        // make sure the separator isn’t at the beginning or end of the line
        if (i != b && i + sep.size() != e) {
            // beg marks the beginning of the protocol-name
            iter beg = i;
            while (beg != b && isalpha(beg[-1]))
                --beg;
            // is there at least one appropriate character before and after the separator?
            if (beg != i && !not_url_char(i[sep.size()]))
                return beg;
        }
        // the separator we found wasn’t part of a URL; advance i past this separator
        i += sep.size();
    }
    return e;
}

// std::accumulate(begin, end, starting_value)
// Computes the sum of given values in range

/*
 * ALGORITHMS, CONTAINERS, ITERATORS
 * Algorithms act on container elements -- they do not act on containers
 * p. 135
 *
 * - erase or insert can invalidate any iterator denoting elements after the one erased or inserted
 * - erase must be a member of vector because it acts directly on the container
 * - partition or remove_if will change which element is denoted by iterator
 */



int main() {
    return 0;
}