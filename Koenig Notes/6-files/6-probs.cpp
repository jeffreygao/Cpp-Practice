//
// Created by gaoje on 5/17/2021.
//
#include <vector>
#include <algorithm> // copy
#include <iostream>
#include <iterator> // ostream_iterator
#include <string>
#include <numeric> // accumulate

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::copy;
using std::accumulate;


void prob() {
    vector<int> u(10, 100);
    vector<int> v;
    copy(u.begin(), u.end(), v.begin());
    /*
     * Does not work, v.begin() == v.end() when empty vector
     * end() is iterator one past so invalid placement
     * AND copy() return value is not assigned
     */
    // copy direct to output -- std::ostream_iterator<int>(std::cout, " ")
    for (int i = 0; i != u.size(); i++) {
        cout << v[i] << endl;
    }
}

string concatenate(const vector<string>& in) {
    string out;
    out = accumulate(in.begin(), in.end(), out);
    return out;
}


int main() {
//    prob();
    vector<string> in(10, "hello");
    string out = concatenate(in);
    cout << out << endl;
    return 0;
}