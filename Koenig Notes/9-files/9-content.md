# Chapter 9: Defnining New Types
(p.170)

## Class Types
```cpp
struct Student_info {
    std::string name;
    double midterm, final;
    std::vector<double> homework;
    
    // member functions
    std::istream& read(std::istream&);
    double grade() const;
};
```

Can let users manipulate data elements directly or only through functions.
Note: variables are fully qualified instead of forcing users to include `using`-declarations.

### Member function:
A function that is a member of a class object

```cpp
istream& Student_info::read(istream& in)
{
    in >> name >> midterm >> final;
    read_hw(in, homework);
    return in;
}
```

The name of this function is within the namespace of the class. No need to pass class object.
Can refer to data elements directly(since it is within the class).

```cpp
double Student_info::grade() const
{
    return ::grade(midterm, final, homework);
}
```

Similarly, this member function will call the already defined grade function which is
not a member of anything. Without it, the compiler would think that it is referring to
Student_info::grade. Also __const__ membre function which cannot change the internal
state of an object.

___Nonmember functions:___ if modifies an object, make a member function. Some cases
can be left as global functions.

## Protection
```cpp
class Student_info {
    public:
        // interface goes here
        double grade() const;
        std::istream& read(std::istream&);
    private:
        // implementation goes here
        std::string name;
        double midterm, final;
        std::vector<double> homework;
};
```
Protection labels defines the accessibility of all members that follow that label.
- class: defaults to private
- struct: defaults to public

### Accessor functions:
Read-only member functions that correspond to a data value. 
Only used for abstract interface of a class since it reveals "hidden data".
Should not be used for data that is not part of a class interface.

## Constructors
Define how objects are initialized. No explicit call allowed. 
- local variable: default-initialized
- side-effect, container: value-initialized
- Identified by having same class name and no return type.

```cpp
Student_info::Student_info(): midterm(0), final(0) { }
```
An explicit default constructor would look like this where the midterm(0) and final(0) indicate the data element name and initial value.
These are called construct initializers.

__Steps during initialization__
1. Implementation allocates memory to hold the object.
2. Initializes the object using initial values as specified in initializer list.
3. Executes the constructor body.

- better to assign value explicitly so as to avoid twice the work in constructor.
