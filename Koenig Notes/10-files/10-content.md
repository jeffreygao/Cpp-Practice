# Chapter 10: Managing memory and low-level data structures
(p. 184)

## Pointers and arrays
Pointer: random-access iterator for accessing elements in arrays

### Pointers
Pointer is a value that represents the address(denotes the part of
computer's memory that contains object).

E.g. object x, object address &x

    & --> address operator

E.g. address p, object *p

    * --> dereference operator
    
Null pointer: initialized with 0

```cpp
int x;
int *p; // *p is declarator; type of p is "pointer to int"
int* p; // also declarator

int* p, q;
// == 
int (*p), q;
// ==
int *p;
int q;
```

### Pointers to functions
```cpp
// If we dereference pointer func, which takes an argument int, we will
// get an int in return.
int (*func)(int);

int next(int n) {
    return n + 1;
}
// equivalent statements
fp = &next;
fp = next;
// equivalent statements
i = (*func)(i);
i = func(i);
```

If a function takes a function as a parameter, the compiler translates the parameter
to be a pointer to a function instead. 

Use typedef to indicate the return type explicitly.

```cpp
typedef double (*analysis_fp)(const vector<Student_info>&);
analysis_fp get_analysis_ptr();
```
Calling `get_analysis_ptr()` will return a pointer. Dereference the result to get a
function that takes a `vector<Student_info>&` and returns a double.

### Array
<cstddef> defines size_t(unsigned).

```cpp
double coords[3];

// value known from compilation time
const size_t NDim = 3;
double coords[NDim];

// name of an array is always a pointer to initial element
// Sets first element of coords to 1.5
*coords = 1.5; 
```

```cpp
vector<double> v;
copy(coords, coords + NDim, back_inserter(v));
// ==
vector<double> v(coords, coords + NDim);
```

Can use `ptrdiff_t` to represent distance between elements(signed).
Cannot construct pointer to element before the beginning of an array.
Can construct pointer pointing to one past an array to indicate the end.

Pointer p and integer n, `p[n] == *(p + n)`.

## String literals

Strings are arrays of const chars. Null character `\0` automatically appended
to end of string.
\<cstring> has function `strlen` which gives the number of characters in a string
not including the null character.
`const char hello[] = { ’H’, ’e’, ’l’, ’l’, ’o’, ’\0’ };`

## Initializing arrays of character pointers

- sizeof returns the size of an element in bytes, not the number of elements
- static: will only compile expressions once

## Arguments to main

Argv: pointer to initial element of array of pointers, one for each argument

Argc: number of pointers in array to which argv points
- first elements represents name by which program is called
- e.g. executable file name

Example:
`say Hello, world`
- argc = 3
- argv: pointers to initial characters of arrays initialized by each word

## 10.5 Reading and writing files

Output streams:
- cerr: immediate output with high overhead 
- clog: running commentary about program

\<fstream>

Contains ifstream and ofstream types for taking in and writing files.
```cpp
int main(int argc, char **argv)
{
    int fail_count = 0;
    // for each file in the input list
    for (int i = 1; i < argc; ++i) {
        ifstream in(argv[i]);
        // if it exists, write its contents, otherwise generate an error message
        if (in) {
            string s;
            while (getline(in, s))
            cout << s << endl;
        } else {
            cerr << "cannot open file " << argv[i] << endl;
            ++fail_count;
        }
    }
    return fail_count;
}
```

## Memory Management

1. __Automatic__:
Local variables: allocated during execution of variable definition
    - pointers to deallocated variables will be invalid

2. __Static__:
Will not deallocate variable memory as long as program is running.
    - points to same object everytime

3. __Dynamic__:
Uses __new__ and __delete__ keywords to get brand new objects for function calls.

### Allocate/dellocate object

T is type of object
new T is expression to allocate an object T(default initialized and yields pointer
to newly allocated object)

T(*args*) to set value.

Object stays around until program ends or delete T is called. Thus, we could not call `delete` but for
efficiency, remove unecessary memory.

```cpp
// allocate an unamed new object of type int, initialized to 42
// cause p to point to the new object.
int* p = new int(42);
// increment
*p++;
delete p;
// delete[] p if it were a vector
```

